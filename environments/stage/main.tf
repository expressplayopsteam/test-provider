module "test-provider" {
  source                    = "../../src/"
  bucket                    = var.bucket
  providers = {
    aws = aws.ireland
  }
}

module "test-provider-ohio" {
  source                    = "../../src/"
  bucket                    = var.bucket
  providers = {
    aws = aws.ohio
  }
}