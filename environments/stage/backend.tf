terraform {
  backend "s3" {
    profile = "intr-stage"
    bucket  = "intr-devops-terraform-state"
    region  = "eu-west-1"
    key     = "intr/test-provider/infra/terraform.tfstate"
  }
}